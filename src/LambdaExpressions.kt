enum class Role { MANAGER, DEV, QA }
data class Employee(val name: String, val role: Role)

fun createEmployees() = listOf(
		Employee("Karasek", Role.MANAGER),
		Employee("Pomikalek", Role.DEV),
		Employee("Srankota", Role.DEV),
		Employee("Helesic", Role.QA)
)

fun lambdaExpressionsExample() {
	var avgDevNameLength = createEmployees()
			.filter({ employee -> employee.role == Role.DEV })
			.map({ employee -> employee.name.length })
			.average()

	avgDevNameLength = createEmployees()
			.filter { it.role == Role.DEV }
			.map { it.name.length }
			.average()

	// Java8 + Guava style
//	avgDevNameLength =
//			average(
//					transform(
//							filter(employees, { employee -> employee.role == Role.DEV }),
//							{ employee -> employee.name.length }
//					)
//			)
}

fun multilineLambdaExpressionsExample() {
	val employeeNamesUppercase = createEmployees().map { employee ->
		val name = employee.name
		name.toUpperCase()
	}
}

fun countDevsBeforeFirstQa(): Int {
	var devCount = 0;

	createEmployees().forEach {
		if (it.role == Role.DEV)
			devCount++

		if (it.role == Role.QA)
			return devCount
	}

	return devCount
}