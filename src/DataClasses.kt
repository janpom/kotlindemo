package dataclasses

data class Circle(val center: Pair<Int, Int>, val diameter: Int)

fun main(args: Array<String>) {
	val smallCircle = Circle(center = Pair(5, 7), diameter = 10)
	val smallCircle2 = Circle(Pair(5, 7), 10)
	val bigCircle = Circle(5 to 7, 100)

	println("smallCircle = $smallCircle")

	println("smallCircle center = ${smallCircle.center}")

	println("smallCircle.hashCode = ${smallCircle.hashCode()}")
	println("smallCircle2.hashCode = ${smallCircle2.hashCode()}")
	println("bigCircle.hashCode = ${bigCircle.hashCode()}")

	println("small circles equal: ${smallCircle == smallCircle2}")
	println("smal and big circle equal: ${smallCircle == bigCircle}")
}