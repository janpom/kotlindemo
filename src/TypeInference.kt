import java.util.*

fun typeInferenceExample() {
	// explicit types
	val ei: Int = 1
	val ed: Double = ei * 2.0
	val eSallaryMap: Map<String, Int> = HashMap<String, Int>()

	// inferred types
	val i = 1
	val d = i * 2.0
	val salaryMap = mapOf("Alice" to 1000, "Bob" to 2000)

	for (person in salaryMap.keySet()) {
		val salary = salaryMap.get(person)
		println("$person's monthly pay is $salary USD")
	}

	fun square(x: Int): Int {
		return x * x;
	}

	val sqr2 = square(2)
}

