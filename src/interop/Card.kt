package interop

enum class Suit {
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
}

enum class Rank(val value: Int) {
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9),
	J(10),
	Q(11),
	K(12),
	A(13)
}

data class Card(val suit: Suit, val rank: Rank)