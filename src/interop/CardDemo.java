package interop;

import java.util.ArrayList;
import java.util.List;

public class CardDemo
{
	public static void main(String[] args)
	{
		List<Card> cards = new ArrayList<Card>();
		cards.add(new Card(Suit.SPADES, Rank.TWO));
		cards.add(new Card(Suit.HEARTS, Rank.TWO));
		cards.add(new Card(Suit.SPADES, Rank.Q));

		Hand hand = new Hand(cards);
		System.out.println("number of spades: " + hand.spadesCount());
	}
}
