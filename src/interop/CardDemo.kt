package interop

fun main(args: Array<String>) {
	val cards = listOf(
			Card(Suit.SPADES, Rank.TWO),
			Card(Suit.HEARTS, Rank.TWO),
			Card(Suit.SPADES, Rank.Q))

	val hand = Hand(cards)
	println("number of spades: ${hand.spadesCount()}")
}