package interop;

import java.util.List;

public class Hand
{
	private List<Card> cards;

	public Hand(List<Card> cards)
	{
		this.cards = cards;
	}

	public int spadesCount()
	{
		int count = 0;

		for (Card card: cards)
		{
			if (card.getSuit() == Suit.SPADES)
				count++;
		}

		return count;
	}
}
