import java.math.BigDecimal

class PaymentProcessingProfile(
		val currency: Int,
		tranKey: Int,
		subsidiary: Int,
		isMemorized: Boolean,
		total: BigDecimal,
		supportsLegacyHoldManagement: Boolean
) {
	init {
		if (currency < 0)
			throw IllegalArgumentException("currency must not be negative")

		if (tranKey < 0)
			throw IllegalArgumentException("tranKey must not be negative")
	}

	fun currencyName(): String {
		when (currency) {
			1 -> return "USD"
			2 -> return "CZK"
			else -> return "N/A"
		}
	}
}

fun createWithBuilder() {
//	val ppp = PaymentProcessingProfileBuilder()
//			.setTranKey(10)
//			.setSubsidiary(13)
//			.setCurrency(1)
//			.setMemorized(false)
//			.setTotal(BigDecimal.TEN)
//			.setSupportsLegacyHoldManagement(false)
//			.build()
}

fun createWithoutBuilder() {
	val ppp1 = PaymentProcessingProfile(
			currency = 1,
			tranKey = 10,
			subsidiary = 13,
			isMemorized = false,
			total = BigDecimal.TEN,
			supportsLegacyHoldManagement = false
	)

	// this also works
	val ppp2 = PaymentProcessingProfile(1, 10, 13, false, BigDecimal.TEN, false)

	println(ppp1.currencyName())
	println(ppp2.currencyName())
}