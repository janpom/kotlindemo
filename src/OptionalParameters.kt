// Java way -- method overloading

fun javaJoin(list: List<String>, separator: String): String {
	return list.joinToString(separator)
}

fun javaJoin(list: List<String>): String {
	return javaJoin(list, ",")
}


// Kotlin way -- optional parameter
fun join(list: List<String>, separator: String = ","): String {
	return list.joinToString(separator)
}


fun joinTest() {
	val myList = listOf("foo", "bar", "blah")

	println(join(myList))
	println(join(myList, ":"))
}