import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class DelegationJava
{
	static class MapAdapter<K, V> implements Map<K, V>
	{
		protected final Map<K, V> map;

		public MapAdapter(Map<K, V> map)
		{
			this.map = map;
		}

		public int size()
		{
			return map.size();
		}

		public boolean isEmpty()
		{
			return map.isEmpty();
		}

		public boolean containsValue(Object value)
		{
			return map.containsValue(value);
		}

		public boolean containsKey(Object key)
		{
			return map.containsKey(key);
		}

		public V get(Object key)
		{
			return map.get(key);
		}

		public V put(K key, V value)
		{
			return map.put(key, value);
		}

		public V remove(Object key)
		{
			return map.remove(key);
		}

		public void putAll(Map<? extends K, ? extends V> m)
		{
			map.putAll(m);
		}

		public void clear()
		{
			map.clear();
		}

		public Set<K> keySet()
		{
			return map.keySet();
		}

		public Collection<V> values()
		{
			return map.values();
		}

		public Set<Map.Entry<K, V>> entrySet()
		{
			return map.entrySet();
		}

		public boolean equals(Object o)
		{
			return map.equals(o);
		}

		public int hashCode()
		{
			return map.hashCode();
		}

		public String toString()
		{
			return map.toString();
		}
	}

	static class LoggingMap<K, V> extends MapAdapter<K, V> implements Map<K, V>
	{
		public LoggingMap(Map<K, V> map)
		{
			super(map);
		}

		@Override
		public V get(Object key)
		{
			System.out.println("retrieved value of " + key);
			return map.get(key);
		}

		@Override
		public V put(K key, V value)
		{
			System.out.println(String.format("stored %s -> %s", key, value));
			return map.put(key, value);
		}
	}
}
