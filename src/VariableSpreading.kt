package variablespreading

fun main(args: Array<String>) {
	val pair = Pair(10, 20)
	val (x, y) = pair
	println("x = $x, y = $y")

	val tripple = Triple(10, 20, 30)
	val (x1, x2, x3) = tripple
	println("$x1, $x2, $x3")

	val languageCoolnessMap = mapOf(
			"Java" to "nice",
			"Python" to "cool",
			Pair("Kotlin", "very cool"))

	val javaCoolness = languageCoolnessMap.get("Java")
	println("Java -> $javaCoolness")

	for ((language, coolness) in languageCoolnessMap)
		println("$language is $coolness")
}