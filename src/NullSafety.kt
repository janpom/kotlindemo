// ? vs !! solution
fun nullSafetyExample() {
	val salaryMap = mapOf("Alice" to 1000, "Bob" to 2000)
	val bobsMontlyPay: Int = salaryMap.get("Bob") ?: throw IllegalArgumentException()
	val bobsYearlyPay: Int = bobsMontlyPay * 12
	println("Bob makes $bobsYearlyPay a year")
}

// safe call ?. and Elvis Operator ?:
fun safeCallsExample() {
	val DEFAULT_FOO_LENGTH = 10
	val foo: String? = null
	val fooLength: Int? = foo?.length
	println(fooLength)
}

// chaining safe calls:
// bob?.department?.head?.name

/*
// NetSuite world example
// Java
NLField myfield = form.getField("myfield")
if (myfield != null)
	myfield.setFlag(NLField.MANDATORY)

// Kotlin
form.getField("myfield")?.setFlag(NLField.MANDATORY)
*/