class NLFieldContainer {}
class NLFormlabelContext {}
class NLSession {}
class NLSelect {}

fun addClassification(
		classification: String,
		addToThis: NLFieldContainer,
		flags: Long,
		nl: NLSession,
		table: String? = null,
		prefix: String? = null,
		lblPrefix: NLFormlabelContext? = null,
		tabname: String? = null,
		addSubsidiarySlaving: Boolean = false
): NLSelect {
	TODO()
}

fun addClassificationTest() {
	val form = NLFieldContainer()
	val nl = NLSession()

	// Java way
	addClassification("department", form, 0, nl, "tranline", null, null, null, false)

	// Kotlin way
	addClassification(
			classification = "department",
			addToThis = form,
			flags = 0,
			nl = nl,
			table = "tranline",
			addSubsidiarySlaving = true
	)
}