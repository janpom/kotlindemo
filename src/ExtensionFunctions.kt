import kotlin.text.Regex

// classic static-method like way
fun TakeFirst(list: List<String>, default: String): String {
	return if (list.size > 0) list.get(0) else default
}

fun Strip(str: String): String {
	return str.replace(Regex("^\\s+"), "").replace(Regex("\\s+$"), "")
}

fun ReplaceAllLetters(str: String, replacement: String): String {
	return str.replace(Regex("[a-zA-Z]"), replacement)
}

// extension functions
fun List<String>.takeFirst(default: String): String {
	return TakeFirst(this, default)
}

fun String.strip(): String {
	return Strip(this)
}

fun String.replaceAllLetters(replacement: String): String {
	return ReplaceAllLetters(this, replacement)
}


fun testClassic() {
	val myList = listOf(" abc ", "efg")
	val firstTrimmedMasked = ReplaceAllLetters(Strip(TakeFirst(myList, "nothing")), "*")
	println(firstTrimmedMasked) // prints "***"
}

fun testExtension() {
	val myList = listOf(" abc ", "efg")
	val firstTrimmedMasked = myList.takeFirst("nothing").strip().replaceAllLetters("*")
	println(firstTrimmedMasked) // prints "***"
}