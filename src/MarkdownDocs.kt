
fun foo() {
	TODO()
}

/**
 * This is pretty much like [foo], but it also takes a [param].
 * We can even reference [the Card class][interop.Card].
 *
 * We don't need to bother with <p> to make a new paragraph.
 *
 * And we can *highlight* things easily.
 *
 * @return some number
 */
fun bar(param: Int): Int {
	TODO()
}