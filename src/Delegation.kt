class LoggingMap<K, V>(val map: MutableMap<K, V>): MutableMap<K, V> by map
{
	override operator fun get(key: K): V? {
		println("retrieved value of $key")
		return map[key]
	}

	override fun put(key: K, value: V): V? {
		println("stored $key -> $value")
		return map.put(key, value)
	}
}

fun <K> mapValuesSum(map: Map<K, Int>): Int
{
	return map.values.sum()
}

fun main(args: Array<String>) {
	val loggingMap = LoggingMap(hashMapOf<String, Int>())
	loggingMap.put("Foo", 1)
	loggingMap.put("Bar", 2)
	val barValue = loggingMap.get("Bar")
	println("Bar value is $barValue")

	val valuesSum = mapValuesSum(loggingMap)
	println("sum of all values is $valuesSum")
}