package stringtemplates

fun main(args: Array<String>) {
	val x = "I am x"
	println("the value of x is '$x'")

	val y = 10
	println("y = $y")

	println("2 * y = ${2 * y}")

	println("""we
can
even
do
multiline
strings

y = $y""")
}
